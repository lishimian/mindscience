"""
This **module** helps to run the molecular dynamics simulation
"""
import sys
from . import run


run(sys.argv)
